pm.test("Status code is 200", function () {
    pm.response.to.have.status(200);
});

let bet1 = [5, 8, 22, 30, 42];
let rest1 = 4;
let bet2 = [2, 9, 17, 31, 43];
let rest2 = 3;

let response = pm.response.json();

let combination = response[0].combinacion.split(" - ");
let last = combination[4].split(" R(");
combination[4] = last[0];
combination=combination.map(Number);
let rest = last[1].substring(0,last[1].length-1);

let match1 = bet1.count(r=> combination.includes(r));
let resultRest1 = rest == rest1 ? 1 : 0;
let result1 = getType(match1, resultRest1);
let match2 = bet2.count(r=> combination.includes(r));
let resultRest2 = rest == rest2 ? 1 : 0;
let result2 = getType(match2, resultRest2);

let price1 = getPrice(response[0].escrutinio, result1, resultRest1);
let price2 = getPrice(response[0].escrutinio, result2, resultRest2);

pm.environment.set("raffleDate", formatDate(response[0].fecha_sorteo));
pm.environment.set("combination", combination.join(" ") + " (" + rest + ")");
pm.environment.set("bet1", bet1.join(" ") + " (" + rest1 + ")");
pm.environment.set("bet2", bet2.join(" ") + " (" + rest2 + ")");
pm.environment.set("price1", price1);
pm.environment.set("price2", price2);

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [day, month, year].join('/');
}

function getType(matchs, rest) {
    switch(matchs) {
      case 5:
        if(rest == 1) return "(5 + 1)";
        else return "(5 + 0)";
        break;
      case 4:
        if(rest == 1) return "(4 + 1)";
        else return "(4 + 0)";
        break;
      case 3:
        if(rest == 1) return "(3 + 1)";
        else return "(3 + 0)";
        break;
      case 2:
        if(rest == 1) return "(2 + 1)";
        else return "(2 + 0)";
        break;
      default:
        if(rest == 1) return "Reintegro";
        else return "";
        break;
    }
}

function getPrice(scrutiny, type, rest) {
    let refund = parseFloat(scrutiny[8].premio.replace(',','.'));
    let price = "0";
    
    if(type !== '') {
        var finalScrutiny = findScrutiny(scrutiny, type);
        if(finalScrutiny !== null) price = finalScrutiny.premio; 
    } 
    
    price = parseFloat(price.replace(',','.'));
    if(rest == 1) price += refund;
    return price;
}

function findScrutiny(scrutiny, type) {
  for (var i = 0; i < scrutiny.length -1; i++) {
    if(scrutiny[i].tipo.includes(type)) return scrutiny[i];
  }
  return null;
}