function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('');
}
var today = new Date();
var yesterday = new Date();
yesterday.setDate(new Date().getDate() - 3);
pm.environment.set("today", formatDate(today));
pm.environment.set("yesterday", formatDate(yesterday));